<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------



Route::domain("qqbot_admin",function(){
    Route::group("/",function(){
        Route::get("","admin/index/index");
        Route::get("welcome","admin/Index/welcome");
        Route::get("login","admin/Login/index");
        Route::post("login/check","admin/Login/check");
        Route::get("setting","admin/Setting/index");
        Route::get("command","admin/Command/index");
        //Route::get("caiji","admin/Index/caiji");
        Route::miss('admin/Index/notFound404');
    });
});


Route::domain("agent",function(){
    Route::group("/v/:api/",function(){
        Route::post("recharge","agent/:api.Recharge/confirm");
        Route::post("refuse_recharge","agent/:api.Recharge/refuse");
        Route::post("drawing","agent/:api.Drawing/confirm");
        Route::post("refuse_drawing","agent/:api.Drawing/refuse");
        Route::get("user/del","agent/:api.User/del");

        Route::miss("admin/Miss/miss");
    });

    Route::group("/",function(){
        Route::get("","agent/Index/index");
        Route::get("welcome","agent/Index/welcome");
        Route::get("login","agent/Login/index");
        Route::get("logout","agent/Login/logout");
        Route::post("login/check","agent/Login/check");
        Route::get("setting","agent/Setting/index");
        Route::post("command","agent/Command/index");
        Route::get("command","agent/Command/index");
        Route::get("recharge","agent/Recharge/index");
        Route::get("drawing","agent/Drawing/index");
        Route::post("shelter","agent/Shelter/index");
        Route::get("shelter","agent/Shelter/index");
        Route::get("agent/edit","agent/agent/edit");
        Route::post("agent/edit","agent/agent/edit");
        Route::get("agent/add","agent/agent/add");
        Route::post("agent/add","agent/agent/add");
        Route::post("agent/del","agent/agent/del");
        Route::get("agent/e_p","agent/agent/updatePassword");
        Route::get("agent/e_p_s","agent/agent/e_p");
        Route::post("agent/e_p_s","agent/agent/e_p");
        Route::get("agent","agent/agent/index");
        Route::get("user/edit","agent/User/edit");
        Route::post("user/edit","agent/User/edit");
        Route::get("user","agent/User/index");
        Route::get('test','agent/Index/index');
        Route::miss('agent/Index/notFound404');

    });
});

