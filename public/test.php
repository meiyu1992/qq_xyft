<?php
if (isset($_GET['url']))
{
    set_time_limit(0);

    $url = trim($_GET['url']);
    $filePath = md5($url).'.png';
    if (is_file($filePath))
    {
        exit($filePath);
    }

    $command = "phantomjs snap.js {$url} {$filePath}";
    @exec($command);

    exit($filePath);
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <title>快照生成-www.oicqzone.com</title>
    <script src="./static/js/jquery.js"></script>
    <style>
        * {
            margin: 0;
            padding: 0;
        }

        form {
            padding: 20px;
        }

        div {
            margin: 20px 0 0;
        }

        input {
            width: 200px;
            padding: 4px 2px;
        }

        #placeholder {
            display: none;
        }
    </style>
</head>

<body>
<form action="" id="form">
    <input type="text" id="url" />
    <button type="submit">生成快照</button>

    <div>
        <img src="" alt="" id="placeholder" />
    </div>
</form>
<script>
    $(function(){
        $('#form').submit(function(){
            if (typeof($(this).data('generate')) !== 'undefined' && $(this).data('generate') === true)
            {
                alert('正在生成网站快照，请耐心等待...');
                return false;
            }

            $(this).data('generate', true);
            $('button').text('正在生成快照...').attr('disabled', true);

            $.ajax({
                type: 'GET',
                url: '?',
                data: 'url=' + $('#url').val(),
                success: function(data){
                    $('#placeholder').attr('src', data).show();
                    $('#form').data('generate', false);
                    $('button').text('生成快照').attr('disabled', false);
                }
            });

            return false;
        });
    });
</script>
</body>
</html>


