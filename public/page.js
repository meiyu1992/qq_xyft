var page = require('webpage').create();
var address = 'http://baidu.com';//填写需要打印的文件位置
var output = './';//存储文件路径和名称
page.viewportSize = { width: 100, height: 100 };//设置长宽
page.open(address, function (status) {
    if (status !== 'success') {
        console.log('Unable to load the address!');
        phantom.exit();
    } else {
        window.setTimeout(function () {
            page.render(output);
            phantom.exit();
        }, 500);
    }
});