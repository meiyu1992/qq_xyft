<?php
/**
 * Created by PhpStorm.
 * User: 269995848@qq.com
 * Date: 2018/9/10
 * Time: 10:33
 */

namespace app\lib\exception;


class MissException extends BaseException{
    public $code=403;
    public $msg = '抱歉，您提交的地址或方式有误！';
    public $error_code = 10001;
}