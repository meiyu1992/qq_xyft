<?php
/**
 * Created by PhpStorm.
 * User: 269995848@qq.com
 * Date: 2018/10/17
 * Time: 15:50
 */

namespace app\lib\exception;


class DeleteFalseException extends BaseException{
    public $code=403;  //HTTP status code ps:404,200
    public $msg='删除失败！';
    public $error_code=10009;
}