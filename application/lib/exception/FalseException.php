<?php
/**
 * Created by PhpStorm.
 * User: 269995848@qq.com
 * Date: 2018/8/22
 * Time: 16:49
 */

namespace app\lib\exception;


use app\lib\enum\SelfConfig;

class FalseException extends BaseException{
    public $error_code=10000;
    public $code=200;  //HTTP status code ps:404,200
    public $msg='我不太懂这个操作！';

}