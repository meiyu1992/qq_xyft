<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/2/14 9:43
 */

namespace app\lib\exception;


class ParameterException extends BaseException{

    public $code=200;  //HTTP status code ps:404,200
    public $msg='parameter error!';
    public $error_code=10000;
}