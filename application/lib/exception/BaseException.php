<?php
/**
 * Created by PhpStorm.
 * User: 269995848@qq.com
 * Date: 2018/8/21
 * Time: 3:11
 */

namespace app\lib\exception;


use think\Exception;
use Throwable;

class BaseException extends Exception{

    public $code=400;  //HTTP status code ps:404,200
    public $msg='parameter error!';
    public $error_code=999;

    public function __construct($params=[]){
        if(!is_array($params)){
            return;
        }
        if(array_key_exists('error_code',$params)){
            $this->error_code=$params['error_code'];
        }
        if(array_key_exists('code',$params)){
            $this->code=$params['code'];
        }
        if(array_key_exists('msg',$params)){
            $this->msg=$params['msg'];
        }

    }

}