<?php
/**
 * Created by PhpStorm.
 * User: 269995848@qq.com
 * Date: 2018/10/20
 * Time: 17:30
 */

namespace app\lib\exception;


class NoneException extends BaseException{
    public $code=200;  //HTTP status code ps:404,200
    public $msg='暂无数据！';
    public $error_code=10001;
}