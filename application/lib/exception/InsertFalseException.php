<?php
/**
 * Created by PhpStorm.
 * User: 269995848@qq.com
 * Date: 2018/10/17
 * Time: 15:37
 */

namespace app\lib\exception;


class InsertFalseException extends BaseException{
    public $code=201;  //HTTP status code ps:404,200
    public $msg='新增失败！';
    public $error_code=10009;
}