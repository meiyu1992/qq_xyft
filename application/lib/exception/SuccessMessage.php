<?php
/**
 * Created by PhpStorm.
 * User: 269995848@qq.com
 * Date: 2018/8/22
 * Time: 14:41
 */

namespace app\lib\exception;


class SuccessMessage extends BaseException{
    public $error_code = 0;
    public $code = 200;
    public $msg = '成功';

}