<?php
/**
 * Created by PhpStorm.
 * User: 269995848@qq.com
 * Date: 2018/10/17
 * Time: 15:27
 */

namespace app\lib\exception;


class UpdateFalseException extends BaseException{
    public $code=200;  //HTTP status code ps:404,200
    public $msg='修改失败！';
    public $error_code=10009;
}