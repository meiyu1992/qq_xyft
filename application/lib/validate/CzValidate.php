<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/2/14 9:29
 */

namespace app\lib\validate;


class CzValidate extends BaseValidate{
    protected $rule=[
        'cz'=>'require|isNotEmpty|length:2,20'
    ];
}