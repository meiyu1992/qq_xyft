<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/2/25 4:12
 */

namespace app\lib\validate;


class QQBotInfoCheckValidate extends BaseValidate{

    protected $rule=[
        'qq'=>'require|isNotEmpty|length:6,15',
        'password'=>'require|length:6,16',
    ];
}