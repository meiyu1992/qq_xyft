<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/2/16 3:34
 */

namespace app\lib\validate;


class SignValidate extends BaseValidate{
    protected $rule=[
        'sign'=>'require|isNotEmpty|length:37'
    ];
}