<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/2/14 9:29
 */

namespace app\lib\validate;


class UidValidate extends BaseValidate{

    protected $rule=[
        'uid'=>'require|isNotEmpty|length:36'
    ];
}