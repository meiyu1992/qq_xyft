<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/2/16 3:37
 */

namespace app\lib\validate;


class SelfCzValidate extends BaseValidate{
    protected $rule=[
        'self'=>'require|isNotEmpty|length:2,20'
    ];
}