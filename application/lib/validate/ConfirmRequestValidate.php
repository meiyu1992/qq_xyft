<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/10 7:09
 */

namespace app\lib\validate;


class ConfirmRequestValidate extends BaseValidate{
    protected $rule=[
        'money'=>'require|isPositiveInteger',
        'from_group'=>'require|isPositiveInteger',
        //'qq'=>'require|isPositiveInteger'
    ];
}