<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/2/25 4:23
 */
namespace app\lib\enum;
class Admin{
    //代理端
    public const QQ_AGENT='qq_admin_agent';
    public const QQ_AGENT_SCOPE= "admin_scope_agent";
    //用户端
    public const QQ_ADMIN_USER='qq_admin_user';
    public const QQ_ADMIN_USER_SCOPE='qq_admin_user_scope';
    //超级后台
    public const SUPER_ADMIN='super_admin';
    public const SUPER_ADMIN_SCOPE='super_admin_scope';

    //public const
}