<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/2/25 6:43
 */

namespace app\lib\enum;


class Url{

    public const LOGIN_CHECK="http://bot.boyi_spider.com/api/getQQBotAgentInfo";



    public const PHANTOM_JS="D:\phtoamjs\phantomjs-2.1.1-windows\bin\phantomjs.exe";

    /**
     * 幸运飞艇
     */
    //采集地址
    public const XYFT_URL='https://api.api68.com/pks/getLotteryPksInfo.do?lotCode=10057';

    //开奖画面
    public const XYFT_IMG='https://1682013.co/view/xingyft/pk10kai_history.html';

    //图片路径
    public const XYFT_LOAD_PATH="D:\QQboot\CQPro\data\image\xyft.jpg";

}