<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/2/25 4:33
 */

namespace app\lib\enum;


class Code{
    //判断状态
    public const SUCCESS = 1;
    public const ERROR = 0 ;
    public const NORMAL =1 ;

    //订单状态
    public const O_SUCCESS=2;
    public const O_FAIL=3;
    public const O_CONFIRM=1;

    //后台系统等级
    public const SUPER_BACKSTAGE=0;
    public const USER_BACKSTAGE=1;
    public const AGENT_BACKSTAGE=2;

    //分页参数
    public const PAGE=1;
    public const SIZE=15;

    //缓存参数
    public const CACHE_EXS=3600*24*30;
    public const CACHE_USER_EXT='usercache_';

    //缓存开奖期数
    public const OPEN_CODE='open_code';
    //开奖状态
    public const OPEN_STATUS='open_status';
    public const OPEN_STATUS_CODE_1=1;  //开奖状态标识

    //封盘状态
    public const STOP_BETTING='stop_betting';
    public const STOP_BETTING_STATUS_1=1;  //开奖状态标识


    //封盘状态标志
    public const QQ_BOT_MESSAGE_STATUS_1=1; //已封盘
    public const QQ_BOT_MESSAGE_STATUS_0=0; //未封盘

    //下注记录缓存标志
    public const GAME_RECORD='game_record';

    //赔率缓存标志
    public const ODDS='odds';
    public const ODDS_NUM=1.98;
}