<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/10 22:14
 */

namespace app\lib\enum;


class FieldsChange{

    //真假用户
    final public static function trueTh():array {
        return [
            1=>'真',
            2=>'假'
        ];
    }

    final public static function status():array {
        return [
            1=>'待确认',
            2=>'成功',
            3=>'失败',
        ];
    }

    final public static function from():array{
        return [
            1=>'Q群',
            2=>'盘口'
        ];
    }

    final public static function platform():array{
        return [
            1=>'支付宝',
            2=>'微信',
            3=>'银行转账',
            4=>'自行交易',
        ];
    }
}