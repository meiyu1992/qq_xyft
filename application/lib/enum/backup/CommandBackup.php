<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/12 11:13
 */

namespace app\lib\enum\backup;


interface CommandBackup{

    //命令备份
    public static function configCommand():array ;

    //防护模式基本配置
    public static function configShield():array ;

    //代理名单配置
    public static function agentList():array;



}