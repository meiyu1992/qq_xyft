<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/12 11:09
 */

namespace app\lib\enum;


use app\lib\enum\backup\CommandBackup;

class QqBot implements CommandBackup{

    public const COMMAND='qqBotCommand';

    //前缀标识符
    public const PREFIX_COMMAND='!';
    //监听端口
    public const API='127.0.0.1:5700';

    public const MASTER='2338834131';

    //解析错误提示
    public const RESOLUTION_M='命令解析失败！请直接输入 帮助 查看帮助';

    //中文解析长度，超过不解析
    public const CHINESE_LEN=19;

    //解析全英文命令
    public const IS_ENG=1;

    //延迟发送模式
    public const RATE_LIMITED='_rate_limited';

    //代理名单
    public const AGENT_LIST='agent_list';

    //保护模式
    public const SHIELD='shield';

    //qq群公告
    public const QQ_NOTICE='qq_notice';

    //下注规则之一
    final public static function betsRule(){
        return ['大','小','单','双'];
    }

    final public static function configCommand():array{
        // TODO: Implement configCommand() method.
        return [
            'num'=>'查分,查看,查',
            'help'=>'帮助,帮',
            'addnum'=>'上分,上,加',
            'drawing'=>'下分,下,加'
        ];
    }


    final public static function configShield():array {
        return [  //1开2关
            'rate'=>1,  //延迟
            'prefix'=>2, //指令前缀
            'qimg'=>1  //是否允许群员发送图片
        ];
    }


    final public static function agentList():array{
        return [
            '331399155'=>'1365392581'
        ];
    }




}