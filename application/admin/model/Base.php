<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/6 16:23
 */

namespace app\admin\model;


use think\Model;

class Base extends Model{
    protected $hidden=['update_time'];
}