<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/2/17 20:53
 */

namespace app\admin\controller;


use app\lib\enum\Admin;
use think\App;
use think\Controller;

class BaseController extends Controller{


    public function initialize() {
        // 判定用户是否登录
        $isLogin = $this->isLogin();
        if(!$isLogin) {
            //halt(1);
            return $this->redirect('/login');
        }

    }



    public function isLogin() {
        //获取session
        $user = session(Admin::QQ_AGENT, '', Admin::QQ_AGENT_SCOPE);

        if($user) {
            return true;
        }

        return false;
    }
}