<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/2/25 4:58
 */

namespace app\admin\controller;


use app\lib\enum\Admin;
use think\Controller;

class Index extends BaseController{


    public function index(){
        return $this->fetch();
    }
    public function welcome(){
        $sessionObj=session(Admin::SUPER_ADMIN, '', Admin::SUPER_ADMIN_SCOPE);
       //$sessionObj);exit;
        return "欢迎管理员：".$sessionObj;
    }
    public function notFound404(){
        return $this->fetch();
    }

    public function caiji(){
        return $this->fetch();
    }
}