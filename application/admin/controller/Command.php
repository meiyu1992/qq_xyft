<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/2/26 5:50
 */

namespace app\admin\controller;


use app\admin\controller\BaseController;

class Command extends BaseController{

    public function index(){
        $data=explode(",",getConfig("command"));
        return $this->fetch('',['data'=>$data]);
    }
}