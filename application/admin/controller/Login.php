<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/2/25 4:30
 */

namespace app\admin\controller;


use app\lib\enum\Admin;
use app\lib\enum\Code;
use app\lib\enum\Salt;
use app\lib\enum\Url;
use app\lib\validate\QQBotInfoCheckValidate;
use think\Controller;
use think\Request;

class Login extends Controller{

    public function index(){
        return $this->fetch();
    }

    public function test(\think\Request $request){

        $data['msg']='success';
        $data['error']=0;
        //halt($request->post('openCode'));
        return returnData($request->post('openCode'));

    }



    public function check(Request $request) {
        (new QQBotInfoCheckValidate())->goCheck();
        if (!captcha_check($request->param('code'))){

            $this->error('验证码不正确!');
        }
        $arr['qq']=$request->param('qq');
        $arr['password']=$request->param('password');

        $data=json_decode(send_post(Url::LOGIN_CHECK,$arr),true);

        if($data['error_code'] !== Code::SUCCESS){
            $this->error($data['msg']);
        }

        $expire=$data['data']['expire_time'];
        Session(Admin::QQ_ADMIN_AGENT, $data['data']['username'], Admin::QQ_Admin_SCOPE);
        $this->success('登录成功', '/',$expire);


    }

}