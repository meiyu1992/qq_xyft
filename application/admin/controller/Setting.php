<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/2/25 22:43
 */

namespace app\admin\controller;


class Setting extends BaseController{

    public function index(){
        $arr=[];
        $arr['IsPrivateChat']=getConfig("isPrivateChat");
        $arr['bot']=getConfig("bot");
        $arr['master']=getConfig("master");
        $arr['welcomeMsg']=getConfig("welcomeMsg");
        $arr['privateMsg']=getConfig("privateMsg");
        $arr['commandPrefix']=getConfig("commandPrefix");
        $arr['atMe']=getConfig("atMe");
        return $this->fetch('',['arr'=>$arr]);
    }
}