<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/29 10:05
 */

namespace app\common;




use app\lib\enum\Url;
use JonnyW\PhantomJs\Client;

class Phantom{


    final public static function xyft():string {
        $client=Client::getInstance();
        $client->getEngine()->setPath(Url::PHANTOM_JS);
        $client->isLazy();

        $request  = $client->getMessageFactory()->createCaptureRequest(Url::XYFT_IMG);
        $request->setTimeout(5000);
        $response = $client->getMessageFactory()->createResponse();

        $request->setCaptureDimensions(615, 65, 715, 210);//WIDTH HEIGHT TOP LEFT

        $request->setOutputFile('./file.jpg');

        $client->send($request, $response);

        rename('./file.jpg',Url::XYFT_LOAD_PATH);

        return 'xyft.jpg';
    }
}