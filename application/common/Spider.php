<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/22 14:54
 */

namespace app\common;


use app\lib\enum\Code;
use app\lib\enum\QqBot;
use app\lib\enum\Url;
use app\qqbot\service\myservice\SendMessage;
use JonnyW\PhantomJs\Client;
use QL\QueryList;
use think\facade\Cache;
use JonnyW\PhantomJs\DependencyInjection\ServiceContainer;
class Spider{

    public function index(){

//        $client=Client::getInstance();
//        $client->getEngine()->setPath(Url::PHANTOM_JS);
//        $client->isLazy();
//
//        $request  = $client->getMessageFactory()->createCaptureRequest('https://1682013.co/view/xingyft/pk10kai_history.html');
//        $request->setTimeout(5000);
//        $response = $client->getMessageFactory()->createResponse();
//
//        $file = './file.jpg';
//        $request->setCaptureDimensions(580, 65, 715, 210);
//        $request->setOutputFile('D:\QQboot\CQPro\data\image\file.jpg');
//
//        $client->send($request, $response);

//        $newFile='D:\QQboot\CQPro\data\image\file.jpg';
//        copy($file,$newFile);
        //Phantom::xyft();exit;
    }

    public static function setHeader(){

        //$jar=new \GuzzleHttp\Cookie\CookieJar;//后续待用
        $initArr=[
            [
                "Proxy-Connection"=>"keep-alive",
                "Pragma"=> "no-cache",
                "Cache-Control"=> "no-cache",
                "User-Agent"=> "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36",
                "Accept"=> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
                "DNT"=> "1",
                "Accept-Encoding"=> "gzip, deflate, sdch",
                "Accept-Language"=>"zh-CN,zh;q=0.8,en-US;q=0.6,en;q=0.4",
                "Referer"=>"https://www.baidu.com/s?wd=%BC%96%E7%A0%81&rsv_spt=1&rsv_iqid=0x9fcbc99a0000b5d7&issp=1&f=8&rsv_bp=1&rsv_idx=2&ie=utf-8&rqlang=cn&tn=baiduhome_pg&rsv_enter=0&oq=If-None-Match&inputT=7282&rsv_t",
                "Accept-Charset"=> "gb2312,gbk;q=0.7,utf-8;q=0.7,*;q=0.7",
                //'jar'   =>$jar
            ],
            [
                "Proxy-Connection"=>"keep-alive",
                "Pragma"=>"no-cache",
                "Cache-Control"=>"no-cache",
                "User-Agent"=>"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.221 Safari/537.36 SE 2.X MetaSr 1.0",
                "Accept"=>"image/gif,image/x-xbitmap,image/jpeg,application/x-shockwave-flash,application/vnd.ms-excel,application/vnd.ms-powerpoint,application/msword,*/*",
                "DNT"=>"1",
                "Referer"=>"https://www.baidu.com/link?url=c-FMHf06-ZPhoRM4tWduhraKXhnSm_RzjXZ-ZTFnPAvZN",
                "Accept-Encoding"=>"gzip, deflate, sdch",
                "Accept-Language"=>"zh-CN,zh;q=0.8,en-US;q=0.6,en;q=0.4",
                //'jar'   =>$jar
            ],

            [
                "Proxy-Connection"=> "keep-alive",
                "Pragma"=>"no-cache",
                "Cache-Control"=> "no-cache",
                "User-Agent"=>"Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0",
                "Accept"=>"image/x-xbitmap,image/jpeg,application/x-shockwave-flash,application/vnd.ms-excel,application/vnd.ms-powerpoint,application/msword,*/*",
                "DNT"=>"1",
                "Referer"=>"https://www.baidu.com/s?wd=http%B4%20Pragma&rsf=1&rsp=4&f=1&oq=Pragma&tn=baiduhome_pg&ie=utf-8&usm=3&rsv_idx=2&rsv_pq=e9bd5e5000010",
                "Accept-Encoding"=>"gzip, deflate, sdch",
                "Accept-Language"=>"zh-CN,zh;q=0.8,en-US;q=0.7,en;q=0.6",
                //'jar'   =>$jar
            ],
            [
                "Proxy-Connection"=>"keep-alive",
                "Pragma"=> "no-cache",
                "Cache-Control"=> "no-cache",
                "User-Agent"=> "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:55.0) Gecko/20100101 Firefox/55.0",
                "Accept"=> "*/*",
                "DNT"=> "1",
                "Referer"=> "https://www.baidu.com/link?url=c-FMHf06-ZPhoRM4tWduhraKXhnSm_RzjXZ-ZTFnP",
                "Accept-Encoding"=> "gzip, deflate, sdch",
                "Accept-Language"=>"zh-CN,zh;q=0.9,en-US;q=0.7,en;q=0.6",
                //'jar'   =>$jar
            ],

            [
                "Connection"=>"keep-alive",
                "Pragma"=> "no-cache",
                "Cache-Control"=>"no-cache",
                "User-Agent"=>"Mozilla/5.0 (Windows NT 10.0; Win64; x64;) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063",
                "Accept"=>"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
                "Referer"=> "https://www.baidu.com/link?url=c-FMHf06-ZPhoRM4tWduhraKXhnSm_RzjXZ-",
                "Accept-Encoding"=> "gzip, deflate, sdch",
                "Accept-Language"=> "zh-CN,zh;q=0.9,en-US;q=0.7,en;q=0.6",
                "Accept-Charset"=> "gb2312,gbk;q=0.7,utf-8;q=0.7,*;q=0.7",
                //'jar'   =>$jar
            ],

            [
                "Accept"=> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
                "Accept-Encoding"=> "gzip, deflate, sdch",
                "Accept-Language"=> "zh-CN,zh;q=0.8",
                "Pragma"=> "no-cache",
                "Cache-Control"=> "no-cache",
                "Connection"=> "keep-alive",
                "DNT"=> "1",
                "Referer"=> "https://www.baidu.com/s?wd=If-None-Match&rsv_spt=1&rsv_iqid=0x9fcbc99a0000b5d7&issp=1&f=8&rsv_bp=1&rsv_idx=2&ie=utf-8&rq",
                "Accept-Charset"=> "gb2312,gbk;q=0.7,utf-8;q=0.7,*;q=0.7",
                "User-Agent"=> "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.221 Safari/537.36 SE 2.X MetaSr 1.0"
            ],
        ];



        return $initArr[array_rand($initArr)];
    }
}