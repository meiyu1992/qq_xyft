<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/4/3 10:24
 * 幸运飞艇开奖结算
 */

namespace app\common\opencode\xyft;


use app\lib\enum\Code;
use app\lib\enum\QqBot;
use think\Facade;

class Lottery{


    /**
     * @param array $openCode 开奖号码 $order 每张订单
     * @return array
     * user:269995848@qq.com  2019/4/3 14:51
     */
    public static function handleLottery(array $openCode,array $order){
        $redisObj=redisObj();

        //开奖测试数据
        //$testOpenCode=['3','1','2','4','10','5','7','4','9','10'];
        //测试赔率
        $pl=$redisObj->get(Code::ODDS);

        $res=0;  //中奖次数计数


        if(in_array($order['rule'][0],QqBot::betsRule())){
            foreach($order['load'] as $k=>$v){
                if($v == $openCode[$v-1]){
                    switch($order['rule'][0]){
                        case '大':
                            if(self::l($v)){
                                $res++;
                            }
                        break;
                        case '小':
                            if(self::s($v)){
                                $res++;
                            }
                        break;
                        case '单':
                            if(self::si($v)){
                                $res++;
                            }
                        break;
                        case '双':
                            if(self::do($v)){
                                $res++;
                            }
                        break;
                    }
                }
            }
        }else{
            foreach($order['load'] as $k=>$v){
                foreach($order['rule'] as $j=>$h){
                    if($h == $openCode[$v-1])$res++;
                }
            }
        }

        $order['netAmount']=$res*$order['singleMoney']*$pl;
        $order['platAmount']=$order['totalMoney']-$order['netAmount'];

        return $order;
    }

    //大 对应车道上的下注号
    private static function l(string $loadRule):bool{
        $arr=['6','7','8','9','10'];
        if(in_array($loadRule,$arr)){
            return true;
        }
        return false;
    }
    //小
    private static function s(string $loadRule):bool{
        $arr=['1','2','3','4','5'];
        if(in_array($loadRule,$arr)){
            return true;
        }
        return false;
    }
    //单
    private static function si(string $loadRule):bool{
        $arr=['1','3','5','7','9'];
        if(in_array($loadRule,$arr)){
            return true;
        }
        return false;
    }
    //双
    private static function do(string $loadRule):bool{

        $arr=['2','4','6','8','10'];
        if(in_array($loadRule,$arr)){
            return true;
        }
        return false;
    }
}