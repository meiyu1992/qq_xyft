<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/4/4 18:25
 * 结算
 */

namespace app\common\opencode\xyft;


use app\lib\enum\Code;
use app\qqbot\model\GameRecord;
use app\qqbot\service\myservice\SendMessage;
use think\Db;
use think\facade\Log;

class Balance{

    public static function index($qun,$openCode){

        $len=redisObj()->lLen(Code::GAME_RECORD);
        $openData=redisObj()->lRange(Code::GAME_RECORD,0,$len);
        //$testOpenCode=['1','2','2','4','10','5','7','4','9','10'];
        $createInsertOrderArr=[];    //入库所有订单
        $returnWinOrderArr=[];  //返回中奖人员名单
        $groupArr=[];  //消息发送的群组
        $updateUserMoneyArr=[];  //批量修改中奖用户的金额

        foreach($openData as $k=>$v){
            $bets=unserialize($v);
            if(empty($bets)){
                die();
            }

            $betsElse=unserialize($bets['reAmount']);
            $betsElse['qq']=$bets['qq'];
            $betsElse['name']=$bets['name'];
            $betsElse['from_group']=$bets['from_group'];
            $betsElse['id']=$bets['member_id'];
            $winData=Lottery::handleLottery($openCode,$betsElse);

            $bets['netAmount']=$winData['netAmount'];
            $bets['platAmount']=$winData['platAmount'];
            $bets['betTime']=date('Y-m-d h:i:s',$bets['betTime']);
            $bets['flag']=1;

            if($winData['netAmount'] != 0){  //说明该注下的有些次数中了，则要返回中奖消息

                $returnWinOrLoseMsg['validBetAmount']=$bets['validBetAmount'];
                $returnWinOrLoseMsg['xzhm']=$bets['xzhm'];
                $returnWinOrLoseMsg['netAmount']=$bets['netAmount'];
                $returnWinOrLoseMsg['qq']=$winData['qq'];
                $returnWinOrLoseMsg['name']=$winData['name'];
                $returnWinOrLoseMsg['from_group']=$winData['from_group'];

                array_push($returnWinOrderArr,$returnWinOrLoseMsg);

                $tempArr=[];
                $tempArr['id']=(int)$winData['id'];
                $tempArr['money']=$winData['netAmount'];
                array_push($updateUserMoneyArr,$tempArr);
                if(!in_array($winData['from_group'],$groupArr)){
                    array_push($groupArr,$winData['from_group']);
                }
            }

            array_push($createInsertOrderArr,$bets);
        }

        $groupMsgArr=[];

        $qh=redisObj()->get(Code::OPEN_CODE)['preDrawIssue'];

        Db::startTrans();
        //处理数据用于批量跟新
        try{
            (new GameRecord())->createGameRecord($createInsertOrderArr);
            foreach($updateUserMoneyArr as $v){
                Db::name('user')->where('id',$v['id'])->inc('money',$v['money'])->update();
            }
            Db::commit();
        }catch(\Exception $e){

            Log::write('订单入库有误:'.$e->getMessage(),'error');
            Db::rollback();
        }

        foreach($returnWinOrderArr as $k => $v){
            if($v['from_group'] == $qun){
                array_push($groupMsgArr,$v);
            }
        }
        SendMessage::sendOpenCodeInfo($qun,$groupMsgArr,$qh);
        redisObj()->del(Code::GAME_RECORD); //删除，等待下一次入库
    }
}