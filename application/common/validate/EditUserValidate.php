<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/4/1 14:38
 */

namespace app\common\validate;


use think\Validate;

class EditUserValidate extends Validate{

    protected $rule=[
        'qq'=>'require|between:10000,24444444444444',
        'trueth'=>'require|in:1,2',
        'money'=>'require|between:1,24444444444444'
    ];
}