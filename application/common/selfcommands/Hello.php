<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/25 13:42
 */

namespace app\common\selfcommands;


use app\common\opencode\xyft\Balance;
use app\common\Phantom;
use app\lib\enum\Code;
use app\lib\enum\QqBot;
use app\lib\enum\Url;
use app\qqbot\service\myservice\SendMessage;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\Output;
use think\facade\Cache;

class Hello extends Command{
    private $redisObj;
    public function __construct(?string $name=null){
        parent::__construct($name);
        set_time_limit(0);
        $this->redisObj=redisObj();
    }

    protected function configure(){
        $this->setName('qqbot_caiji')
            ->addArgument('start', Argument::OPTIONAL, "开启采集")
            ->setDescription('执行采集');
    }

    protected function execute(Input $input, Output $output){

        switch($name=$input->getArgument('start')){
            case 'start':
                try{

                    $this->c($input,$output);
                }catch(\Exception $e){
                    $this->c($input,$output);
                }
                break;
            default :
                $output->writeln('try to "php think foreachSpider [your spiderName]"');break;
        }
    }

    //报错后不要停止，继续执行
    private function c($input,$output){

        while(true){
            sleep(1);

            if(time()>=strtotime('Today 04:04:00') && time()<=strtotime('Today 13:09:00')){//现在处于休战状态
                if($this->redisObj->get(Code::OPEN_STATUS)){
                   if($agentList=$this->redisObj->get(QqBot::AGENT_LIST)){
                       foreach($agentList as $k=>$v){
                           SendMessage::groupBan($k,true);
                       }
                   }
                    $this->redisObj->set(Code::OPEN_STATUS,'');//删除标志
                }
                $output->writeln('休战中。。。。。。。');
                continue;
            }else{ //现在处于开战状态
                if(!$this->redisObj->get(Code::OPEN_STATUS)){
                    SendMessage::groupBan($k,false);
                }
                $this->redisObj->set(Code::OPEN_STATUS,Code::OPEN_STATUS_CODE_1);
                if(!$openCodeArr=json_decode(send_get(Url::XYFT_URL),true)){
                    $output->writeln('幸运飞艇采集错误，准备重连。。。。。。。');
                    continue;
                }

                $openCodeArr=$openCodeArr['result']['data'];

                //是否要封盘
                $time=strtotime($openCodeArr['drawTime']) - time();
                if($time <= 40 && $time >= 0){
                    if($this->redisObj->get(Code::STOP_BETTING)){
                        $output->writeln('离开奖还剩40S，开始封盘!!!!!!!!!!!!!!!!!!!!');
                        //TODO 向Q群发送消息封盘消息
                        if($agentList=$this->redisObj->get(QqBot::AGENT_LIST)){
                            foreach($agentList as $k=>$v){
                                SendMessage::stopBetting($k,$openCodeArr['drawIssue']);
                            }
                        }
                        $this->redisObj->set(Code::STOP_BETTING,'');
                    }
                }else{
                    $this->redisObj->set(Code::STOP_BETTING,Code::STOP_BETTING_STATUS_1);
                }


                //判断开奖期号是否对得上
                if($cacheOpenCode=$this->redisObj->get(Code::OPEN_CODE)){
                    //说明期号对得上
                    if($openCodeArr['preDrawIssue'] -1 == $cacheOpenCode['preDrawIssue'] && isset($openCodeArr['preDrawIssue'])){

                        $output->writeln('幸运飞艇 ['.$openCodeArr['preDrawIssue'].'] 采集成功！..................');
                        $this->redisObj->set(Code::OPEN_CODE,$openCodeArr);
                        //  在这里发送QQBOT
                        if($agentList=$this->redisObj->get(QqBot::AGENT_LIST)){
                            foreach($agentList as $k=>$v){
                                SendMessage::sendOpenCode($k,$openCodeArr);
                                SendMessage::sendOpenCodeImg($k,Phantom::xyft());
                                $openCode=explode(",",$openCodeArr['preDrawCode']);
                                //结算
                                Balance::index($k,$openCode);
                            }
                        }

                    }elseif($openCodeArr['preDrawIssue']  == $cacheOpenCode['preDrawIssue']){
                        $next=$openCodeArr['preDrawIssue']+1;
                        $output->writeln('幸运飞艇 ['.$next.'] 正在采集！..................');
                    }else{
                        $output->writeln('幸运飞艇 掉期..................');
                        //狠随意的补期
                        $this->redisObj->set(Code::OPEN_CODE,$openCodeArr);
                    }
                }else{
                    $output->writeln('缓存失败！');
                }
            }
        }
    }
}