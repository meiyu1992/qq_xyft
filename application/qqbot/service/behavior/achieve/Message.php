<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/3 18:46
 */

namespace app\qqbot\service\behavior\achieve;


use app\qqbot\service\command\Help;
use app\qqbot\service\command\Index;

class Message extends MessageRoute{

    protected $event;

    public function route(array $event){

        $this->event=$event;
        return $this->messageHandler($this->event);
    }

    private function messageHandler($event){
        return $this->isPrefix($event);
    }


}