<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/3 18:47
 */

namespace app\qqbot\service\behavior\achieve;


use app\qqbot\service\event\SendMessageEvent;
use app\qqbot\service\src\CQCode;

class Notice{

    public function route(array $event){
        //自己离群
        if(($event['sub_type']=="leave") && ($event['notice_type']=="group_decrease")){
            return $this->groupDecrease($event);
        }

        //管理员同意加群申请
        if(($event['notice_type']=='group_increase') && ($event['sub_type']=='approve')){
            return $this->groupIncrease($event);
        }

        die();
    }

    private function groupDecrease(array $event){
        return SendMessageEvent::sendBack("群友".$event['user_id']."静悄悄的离开了我们，走好，不送！",$event);
    }

    private function groupIncrease(array $event){
        return SendMessageEvent::sendBack(CQCode::At($event['user_id'])."欢迎来到本群，祝你玩得开心",$event);
    }


}