<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/1 9:44
 */

namespace app\qqbot\service\behavior\Definition;



use app\qqbot\service\src\Config;

interface KjBotDefinitionApi{
    //所有的api均由接口提供

    public const send_private_msg = '/send_private_msg';
    public const send_private_msg_async = '/send_private_msg_async';
    public const send_group_msg = '/send_group_msg';
    public const send_group_msg_async = '/send_group_msg_async';
    public const send_discuss_msg = '/send_discuss_msg';
    public const send_discuss_msg_async = '/send_discuss_msg_async';
    public const send_msg = '/send_msg';
    public const send_msg_async = '/send_msg_async';
    public const delete_msg = '/delete_msg';
    public const send_like = '/send_like';
    public const set_group_kick = '/set_group_kick';
    public const set_group_ban = '/set_group_ban';
    public const set_group_anonymous_ban = '/set_group_anonymous_ban';
    public const set_group_whole_ban = '/set_group_whole_ban';
    public const set_group_admin = '/set_group_admin';
    public const set_group_anonymous = '/set_group_anonymous';
    public const set_group_card = '/set_group_card';
    public const set_group_leave = '/set_group_leave';
    public const set_group_special_title = '/set_group_special_title';
    public const set_discuss_leave = '/set_discuss_leave';
    public const set_friend_add_request = '/set_friend_add_request';
    public const set_group_add_request = '/set_group_add_request';
    public const get_login_info = '/get_login_info';
    public const get_stranger_info = '/get_stranger_info';
    public const get_group_list = '/get_group_list';
    public const get_group_member_info = '/get_group_member_info';
    public const get_group_member_list = '/get_group_member_list';
    public const get_cookies = '/get_cookies';
    public const get_csrf_token = '/get_csrf_token';
    public const get_credentials = '/get_credentials';
    public const get_record = '/get_record';
    public const get_status = '/get_status';
    public const get_version_info = '/get_version_info';
    public const set_restart = '/set_restart';
    public const set_restart_plugin = '/set_restart_plugin';
    public const clean_data_dir = '/clean_data_dir';
    public const clean_plugin_log = '/clean_plugin_log';
    public const _get_friend_list = '/_get_friend_list';
    public const _get_group_info = '/_get_group_info';
    public const _get_vip_info = '/_get_vip_info';
    public const __check_update = '/.check_update';
    public const __handle_quick_operation = '/.handle_quick_operation';

}