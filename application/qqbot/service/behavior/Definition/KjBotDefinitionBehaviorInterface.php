<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/1 9:46
 */

namespace app\qqbot\service\behavior\Definition;


interface KjBotDefinitionBehaviorInterface{



    public function getData();//接受数据

    public function handleData();//处理数据

    public function sendData();//发送数据

}