<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/5 13:55
 *
 * 命令交换，中文交换英文，英文交换中文
 */

namespace app\qqbot\service\extend;


use app\lib\enum\Command;
use app\lib\enum\QqBot;
use app\qqbot\service\src\Config;

class ChangeCommand{

    /**
     * 指令目录解析
     * @param string $str
     * @return string
     * user:269995848@qq.com  2019/3/7 20:52
     */
    public static function catalogueParse(string $str,$separator=""):?string{
        $command=redisObj()->get(QqBot::COMMAND);

        foreach($command as $k=>$v){
            if($k==$str){
                return $v.$separator;
            }
        }
        return null;
    }

    /**获取中文规则
     * @param string $str
     * @return string
     * user:269995848@qq.com  2019/3/5 14:31
     */
    public static function parseCommand(string $str):?array{
        $arr=['command'=>'',  //指令
            'rule'=>''     //规则
        ];

        if(preg_match("/^[\x{4e00}-\x{9fa5}]+/u",$str,$chinese)){
            $chineseLen=strlen($chinese[0]);
            if($chineseLen>QqBot::CHINESE_LEN){
                return $arr;
            }
            $arr['rule']=trim(substr($str,$chineseLen));
            $arr['command']=$chinese[0];

        }else{
            return $arr;
        }

        $gbk=redisObj()->get(QqBot::COMMAND);
        foreach($gbk as $k=>$v){
            if(strpos($v,$arr['command'])!==false){
                $arr['command']=$k;
                break;
            }
        }
        return $arr;
    }

}



