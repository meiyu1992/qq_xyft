<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/10 6:09
 * 消息发送
 */

namespace app\qqbot\service\myservice;


use app\lib\enum\Code;
use app\lib\enum\QqBot;
use app\qqbot\service\event\Message;
use app\qqbot\service\event\SendMessageEvent;
use app\qqbot\service\src\CQCode;
use app\qqbot\service\src\MessageSender;
use think\facade\Log;

class SendMessage{


    //上分
    public static function addNumApplyFromGroup($qq,$fromGroup,$addJiFen,$curJiFen):?bool {

        $beforeJiFen=(int)$curJiFen-(int)$addJiFen;
$msg=<<<EOT
上分[$addJiFen]成功,调整前:[$beforeJiFen],调整后:[$curJiFen]
EOT;
        $messageObj=new Message(CQCode::At($qq).$msg,$fromGroup,true);
        $msgSender=new MessageSender();
        try{
            $msgSender->send($messageObj);

        }catch(\Exception $e){
            return null;

        }
        return true;
    }

    /**下分
     * @param int $qq
     * @param int $fromGroup
     * @param int $reduceJiFen
     * @param int $curJiFen
     * @return bool|null
     * user:269995848@qq.com  2019/3/11 9:28
     */
    public static function reduceApplyFromGroup($qq,int $fromGroup,int $reduceJiFen,int $curJiFen,$master):?bool {
        $afterJiFen=(int)$curJiFen+(int)$reduceJiFen;
        $msg=<<<EOT
下分[$reduceJiFen]正在处理....
EOT;
        $messageObj=new Message(CQCode::At($qq).$msg,$fromGroup,true);
        $messageObjMaster=SendMessageEvent::sendMaster('qq用户：['.$qq.']'.' 请求下分'.$reduceJiFen.'，请处理！',$master);
        $msgSender=new MessageSender();
        try{
            $msgSender->send($messageObj);
            $msgSender->send($messageObjMaster);
        }catch(\Exception $e){
            return null;
        }
        return true;
    }

    /**下分提示
     * @param int $qq
     * @param int $fromGroup
     * @param int $reduceJiFen
     * @param int $curJiFen
     * @return bool|null
     * user:269995848@qq.com  2019/3/11 9:28
     */
    public static function reduceAfterApplyFromGroup($qq,int $fromGroup,int $reduceJiFen,int $curJiFen):?bool {
        $afterJiFen=(int)$curJiFen+(int)$reduceJiFen;
        $msg=<<<EOT
下分[$reduceJiFen]处理成功,调整前:[$afterJiFen],调整后:[$curJiFen]
EOT;
        $messageObj=new Message(CQCode::At($qq).$msg,$fromGroup,true);
        $msgSender=new MessageSender();
        try{
            $msgSender->send($messageObj);

        }catch(\Exception $e){
            return null;
        }
        return true;
    }

    /**
     * @param $qq
     * @param int $fromGroup
     * @param int $reduceJiFen  下注分
     * @param int $curJiFen 账户剩余积分
     * @return bool|null
     * user:269995848@qq.com  2019/3/21 20:05
     */
    public static function betsAfterApplyFromGroup($qq,int $fromGroup,int $reduceJiFen,array $message):?bool {
        $msg='';
        $q=redisObj()->get(Code::OPEN_CODE)['preDrawIssue'];
        foreach($message as $v){
            $msg.=$v."\r";
        }
        $returnMsg=<<<EOT
第[$q],下注[$reduceJiFen]处理成功
EOT;
        $msg="\r".$msg."\r".$returnMsg;

        $messageObj=new Message(CQCode::At($qq).$msg,$fromGroup,true);
        $msgSender=new MessageSender();
        try{
            $msgSender->send($messageObj);

        }catch(\Exception $e){
            Log::write('下注后提示消息发送失败！','error');
        }
        exit();
    }

    //封盘后，停止本期下注
    public static function stopBetting($fromGroup,$qh){
        $returnMsg=<<<EOT
第[ $qh ] 期距离封盘时间还剩40S
EOT;
        $messageObj=new Message($returnMsg,$fromGroup,true);
        $msgSender=new MessageSender();
        try{
            $msgSender->send($messageObj);

        }catch(\Exception $e){
            Log::write('封盘消息发送失败！'.$e->getMessage(),'error');
        }
    }

    //发图片
    public static function sendOpenCodeImg($fromGroup,$imgPath){

        $messageObj=new Message(CQCode::Image($imgPath),$fromGroup,true);
        $msgSender=new MessageSender();
        try{
            $msgSender->send($messageObj);

        }catch(\Exception $e){
            Log::write('img发送失败！'.$e->getMessage(),'error');
        }

    }

    //禁言
    public static function groupBan($fromGroup,$enable){
        $messageObj=new Message('',$fromGroup);
        $msgSender=new MessageSender();
        try{
            $msgSender->groupBanList($messageObj,$enable);
        }catch(\Exception $e){
            Log::write('禁言发送失败！'.$e->getMessage(),'error');
        }
    }

    /**开奖结果发送
     * @param $fromGroup
     * @param $openCodeInfoArr
     * @return bool
     * user:269995848@qq.com  2019/3/28 14:55
     */
    public static function sendOpenCode($fromGroup,$openCodeInfoArr){
        if(is_array($openCodeInfoArr)){
            $qh=$openCodeInfoArr['preDrawIssue'];
            $openCode=$openCodeInfoArr['preDrawCode'];
            $msg="\r";
            $msg.=<<<EOT
第 $qh 期 开奖结果：\r
$openCode
EOT;

            $messageObj=new Message($msg,$fromGroup,true);
            $msgSender=new MessageSender();
            try{
                $msgSender->send($messageObj);
            }catch(\Exception $e){
                Log::write('发送开奖数据失败'.$e->getMessage(),'error');
            }

        }else{
            return false;
        }

    }

    //发送结算信息


    public static function sendOpenCodeInfo($fromGroup,$sendInfoArr,$qh):?bool {
        $msg='';
        foreach($sendInfoArr as $v){
            $msg.="中奖昵称:".$v['name']."\r"."总下:".$v['validBetAmount']."\r"."下注号码:".$v['xzhm']."\r"."结算:".$v['netAmount']."\r";
        }
        $returnMsg=<<<EOT
第 [$qh] 开奖结算：\r
$msg
EOT;
        $messageObj=new Message($returnMsg,$fromGroup,true);
        $msgSender=new MessageSender();
        try{
            $msgSender->send($messageObj);

        }catch(\Exception $e){
            return null;
        }
        return true;
    }


}