<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/5 10:55
 */

namespace app\qqbot\service\command;


use app\qqbot\service\event\Message;
use app\qqbot\service\event\SendMessageEvent;
use app\qqbot\service\extend\ChangeCommand;
use app\qqbot\service\src\CQCode;
use think\console\command\optimize\Config;

class Help{

    public function index(array $event):?Message{
        $command=$this->allCommand();
$msg=<<<EOT
    可用命令：
    {$command}
EOT;
        return SendMessageEvent::sendBack(CQCode::Face(21).$msg,$event);
    }

    private function allCommand():string {
        $handle=opendir(dirname(__FILE__).DIRECTORY_SEPARATOR);
        $list='';

        while($file=readdir($handle)){
            if(($file<=>".") && ($file<=>"..")){
                $str=substr($file,0,strpos($file,"."));
                $list.="\r".ChangeCommand::catalogueParse(lcfirst($str));
            }
        }
        closedir($handle);
        return $list;
    }

    //该方法供后台调用
    public static function commandTable(){
        $handle=opendir(dirname(__FILE__).DIRECTORY_SEPARATOR);
        $list=[];

        while($file=readdir($handle)){
            if(($file<=>".") && ($file<=>"..")){
                $str=substr($file,0,strpos($file,"."));
                $list[]=ChangeCommand::catalogueParse(lcfirst($str),",");
            }
        }
        closedir($handle);
        return $list;
    }
}