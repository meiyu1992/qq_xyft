<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/5 19:24
 */

namespace app\qqbot\service\command;


use app\lib\enum\Admin;
use app\lib\enum\Code;
use app\lib\enum\QqBot;
use app\lib\exception\InsertFalseException;
use app\qqbot\model\Recharge;
use app\qqbot\service\event\Message;
use app\qqbot\service\event\SendMessageEvent;
use app\qqbot\service\myservice\SendMessage;
use app\qqbot\service\src\CQCode;
use think\facade\Cache;
use think\facade\Log;

class Addnum{


    /**
     * @param array $event 事件
     * @param string $rule 规则
     * @return Message|null
     * user:269995848@qq.com  2019/3/8 15:15
     */
    public function index(array $event,string $rule){

        return self::addNums($event,$rule);

    }

    /**
     * @param array $event
     * @param string $rule
     * @return Message
     * @throws InsertFalseException
     * user:269995848@qq.com  2019/3/8 17:37
     */
    private static function addNums(array $event,string $rule){
        $qq=$event['user_id'];
        $money=(int)$rule;


        if(!isInt($money))
            return SendMessageEvent::sendBack(CQCode::At($qq)."s请求解析错误,请尝试 '上f 数字'",$event);
        $fromGroup=$event['group_id'];
        $qqName=$event['sender']['nickname'];

        if(!Recharge::addMoneyRequest($money,$qq,$fromGroup,$qqName)){
            throw new InsertFalseException(['msg'=>'Q群用户上分失败！']);
        }

        $message[]=SendMessageEvent::sendMaster('qq用户：['.$qq.']'.' 请求s'.$money.'，请到后台处理！',$event['agent_id']);
        $message[]=SendMessageEvent::sendBack(CQCode::At($qq)."s请求已收到，正在处理",$event);
        return $message;
    }






}