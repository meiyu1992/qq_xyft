<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/8 19:11
 */

namespace app\qqbot\service\command;


use app\lib\exception\FalseException;
use app\qqbot\model\Recharge;
use app\qqbot\service\event\Message;
use app\qqbot\service\event\SendMessageEvent;
use app\qqbot\service\myservice\SendMessage;
use app\qqbot\service\src\CQCode;

class Drawing{

    public function index(array $event,string $rule):?Message{

        return self::drawing($event,$rule);
    }

    public static function drawing($event,$rule){
        $qq=(int)floatval($event['sender']['user_id']);
        if(!isInt($rule)){
            return SendMessageEvent::sendBack(CQCode::At($event['sender']['user_id'])."x参数错误,请尝试 '下分 数字'",$event);
        }
        if(!$user=\app\qqbot\model\Drawing::reduceNum($rule,$event)){
            return SendMessageEvent::sendBack(CQCode::At($event['sender']['user_id'])."x异常，请联系管理",$event);
        }

        if($user instanceof Message){
            return $user;
        }

        if(!$status=SendMessage::reduceApplyFromGroup($qq,$event['group_id'],$rule,$user->money,$event['agent_id'])){
            throw new FalseException(['msg'=>'消息回调失败']);
        }
    }


}