<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/5 16:48
 */

namespace app\qqbot\service\command;


use app\qqbot\model\QqUser;
use app\qqbot\model\User;
use app\qqbot\service\event\Message;
use app\qqbot\service\event\SendMessageEvent;
use app\qqbot\service\src\CQCode;

class Num{

    public function index(array $event):?Message{
        return $this->handlerData($event);
    }

    public function handlerData(array $event){

        $user=User::getUserData(floatval($event['user_id']));

        if(!$user){
            return SendMessageEvent::sendBack(CQCode::At($event['user_id'])."您目前没有积分",$event);
        }

        return SendMessageEvent::sendBack(CQCode::At($event['user_id'])."您当前的积分是:".sprintf("%.2f",$user->money),$event);
    }
}