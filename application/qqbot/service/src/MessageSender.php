<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/4 21:00
 */

namespace app\qqbot\service\src;


use app\lib\enum\QqBot;
use app\qqbot\service\event\Message;
use think\facade\Log;

class MessageSender{
    public $CQ;

    public function __construct(){
        $this->CQ=new CoolQ(QqBot::API);

    }

    /**
     * 发送一条消息
     * @param  $message
     */
    public function send(Message $message){
        if($message->toGroup){
            if($message->async){
                $this->CQ->sendGroupMsgAsync($message->id, $message->msg, $message->auto_escape);
            }else{
                $this->CQ->sendGroupMsg($message->id, $message->msg, $message->auto_escape);
            }
        }else{
            if(isset($message->async) && $message->async){

                $this->CQ->sendPrivateMsgAsync($message->id, $message->msg, $message->auto_escape);
            }else{

                $this->CQ->sendPrivateMsg($message->id, $message->msg, $message->auto_escape);
            }
        }
    }

    /**全体禁言
     * @param Message $message
     * @param bool $enable
     * @return bool
     * user:269995848@qq.com  2019/3/28 11:18
     */
    public function groupBanList(Message $message,bool $enable){
        if($message->toGroup){

            $this->CQ->setGroupWholeBan($message->id,$enable);
        }
        return false;
    }
}