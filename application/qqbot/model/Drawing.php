<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/8 23:41
 */

namespace app\qqbot\model;


use app\qqbot\service\event\SendMessageEvent;
use app\qqbot\service\src\CQCode;
use think\Db;

class Drawing extends Base{


    //下分
    public static function reduceNum($num,array $event){
        $qq=floatval($event['sender']['user_id']);
        $qqName=$event['sender']['nickname'];
        $from_group=$event['group_id'];
        if(!$user_id=self::RegisterCache($qq,$from_group,false)){

            return null;
        }
        $user=User::get($user_id);
        if($user->money - $num < 0){
            return SendMessageEvent::sendBack(CQCode::At($qq)."您当前的jf不足以x[".$num."]",$event);
        }

        Db::startTrans();
        try{
            self::create(['qq'=>$qq,'member_id'=>$user_id,'money'=>$num,'bill_no'=>makeOrderNo(),'name'=>$qqName,'from_group'=>$from_group]);
            Db::name('user')->where('id',$user_id)->dec('money',$num)->update();

            Db::commit();
            return $user;
        }catch(\Exception $e){
            Db::rollback();

            return null;
        }

    }
}