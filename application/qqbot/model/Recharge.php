<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/6 11:33
 */

namespace app\qqbot\model;


use app\lib\enum\Code;
use app\lib\exception\InsertFalseException;
use think\facade\Cache;
use think\facade\Log;

class Recharge extends Base{

    /**qq群上分请求
     * @param int $money
     * @param int $userName QQ号
     * @param int $fromGroup QQ群号
     * @param int $admin_id 管理员账号
     * @return bool
     * user:269995848@qq.com  2019/3/6 15:10
     */

    public static function addMoneyRequest($money,$qq,$fromGroup,$qqName):bool {


        if(!$user_id=self::RegisterCache($qq,$fromGroup)){

            return false;
        }

        $arr=[
            'bill_no'=>makeOrderNo(),
            'from_group'=>$fromGroup,
            'member_id'=>$user_id,
            'name'=>$qqName,
            'payment_type'=>4,
            'qq'=>$qq,
            'money'=>$money,
        ];

        if(!self::create($arr)){

            return false;
        }

        return true;
    }
}