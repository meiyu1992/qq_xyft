<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/6 14:40
 */

namespace app\qqbot\model;


use think\facade\Log;

class User extends Base{

    public static function getUserData($user_qq){
        $user=self::where("qq",$user_qq)->find();

        if(!empty($user)){
            return $user;
        }

        return null;
    }

    public static function register($qq,$form_group){
        $qq=floatval($qq);
        try{
            $user=self::create(['qq'=>$qq,'from_group'=>$form_group]);
        }catch(\Exception $e){

            Log::write('qq用户注册失败：'.$e->getMessage(),'error');
        }

        if($user){

            return $user;
        }


        return null;
    }
}