<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/6 11:34
 */

namespace app\qqbot\model;


use app\lib\enum\Code;
use think\facade\Cache;
use think\facade\Log;
use think\Model;
use think\model\concern\SoftDelete;

class Base extends Model{
    use SoftDelete;
    protected $hidden=['update_time'];


    /**qq群注册用户缓存
     * @param int $qq
     * @param int $form_group
     * @param bool $turn  在不是我们的用户的情况下，是否去注册
     * @return int|null 用户id
     * user:269995848@qq.com  2019/3/11 2:51
     */
    public static function RegisterCache($qq,string $form_group,$turn=true) {

        try{
            if(!$user_id=Cache::get(Code::CACHE_USER_EXT.$qq)){   //缓存不存在，有可能是被删除了，需要去数据库查看该用户是否存在

                if(!$turn){
                    if(!$user=User::getUserData($qq)){ //说明不是我们的用户
                        return null;
                    }else{
                        return $user->id;
                    }
                }
                if(!$user=User::getUserData($qq)){

                    $newUser=User::register($qq,$form_group);
                    $member_id=$newUser->id;

                }else{
                    $member_id=$user->id;
                }

                if(!Cache::set(Code::CACHE_USER_EXT.$qq,$member_id,Code::CACHE_EXS)){
                    return null;
                }
            }else{

                $member_id=$user_id;
            }


            return $member_id;
        }catch(\Throwable $e){

            return null;
        }



    }
}