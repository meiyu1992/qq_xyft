<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/6 14:11
 */

namespace app\agent\controller;


use app\agent\model\User;
use app\lib\enum\Admin;
use app\lib\enum\Salt;
use app\lib\validate\QQBotInfoCheckValidate;
use think\Controller;
use think\facade\Cache;
use think\Request;

class Login extends Controller{
    public function index(){
        return $this->fetch();
    }

    public function test(\think\Request $request){

        $data['msg']='success';
        $data['error']=0;
        //halt($request->post('openCode'));
        return returnData($request->post('openCode'));

    }



    public function check(Request $request) {
        (new QQBotInfoCheckValidate())->goCheck();
        if (!captcha_check($request->param('code'))){

            $this->error('验证码不正确!',"login");
        }
        $arr['qq']=$request->param('qq');
        $arr['password']=md5($request->param('password').Salt::LOGIN_AGENT);

        if(!$user=\app\agent\model\Admin::login($arr['qq'],$arr['password'])){
            $this->error('账号或者密码错误！');
        }
        Cache::set('admin_name',$user->name);
        Session(Admin::QQ_AGENT, $user->name, Admin::QQ_AGENT_SCOPE);
        $this->success('登录成功', '/');

    }

    public function logout(){
        session(null, Admin::QQ_AGENT_SCOPE);
        // 跳转
        $this->redirect('login');
    }
}