<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/12 19:05
 */

namespace app\agent\controller;


use app\lib\enum\QqBot;

class Shelter extends BaseController{

    public function index(\think\Request $request){

        if($request->isPost()){
            $arr=[
                'rate'=>1,
                'prefix'=>1,
                'qimg'=>1
            ];

            if(!$request->has('rate'))
                $arr['rate']=2;
            if(!$request->has('prefix'))
                $arr['prefix']=2;
            if(!$request->has('qimg'))
                $arr['qimg']=2;


            if(!redisObj()->set(QqBot::SHIELD,$arr)){
                $this->error('未知错误，请联系管理员！');
            }

        }

        $data=redisObj()->get(QqBot::SHIELD);

        $this->assign([
            'rate'=>$data['rate'],
            'prefix'=>$data['prefix'],
            'qimg'=>$data['qimg']
            ]);
        return $this->fetch();
    }
}