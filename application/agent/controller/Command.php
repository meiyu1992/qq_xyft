<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/2/26 5:50
 */

namespace app\agent\controller;


use app\admin\controller\BaseController;
use app\lib\enum\QqBot;
use think\facade\Request;
use app\lib\enum\Command as CommandConfig;
class Command extends BaseController{

    public function index(Request $request){

        if($request::isPost()){

            if(!redisObj()->set(QqBot::COMMAND,$request::post())){
                $this->error('未知错误，请联系管理员！');
            }
        }

        $data=redisObj()->get(QqBot::COMMAND);

        return $this->fetch('',['data'=>$data]);
    }


}