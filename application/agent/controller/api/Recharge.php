<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/9 4:06
 */

namespace app\agent\controller\api;


use app\lib\enum\Code;
use app\lib\exception\FalseException;
use app\lib\exception\SuccessMessage;
use app\lib\exception\UpdateFalseException;
use app\lib\validate\ConfirmRequestValidate;
use app\lib\validate\IDMustBePositiveInt;
use app\qqbot\service\myservice\SendMessage;
use think\Controller;
use app\agent\model\Recharge as RechargeModel;
use think\facade\Request;

class Recharge extends Base{


    public function confirm(\think\Request $request):SuccessMessage{

        (new IDMustBePositiveInt())->goCheck();
        (new ConfirmRequestValidate())->goCheck();
        $request=$request->post();

        $user=RechargeModel::confirmRecharge($request);

        if(!$user){
            throw new UpdateFalseException();
        }

        $status=SendMessage::addNumApplyFromGroup($request['qq'],$request['from_group'],$request['money'],$user->money);

        if(!$status){
            throw new FalseException(['msg'=>'消息回调失败']);
        }

        return new SuccessMessage();
    }

    public function refuse(\think\Request $request){
        (new IDMustBePositiveInt())->goCheck();

        $status=RechargeModel::where(['id'=>$request->post('id')])->update(['status'=>Code::O_FAIL]);
        if(!$status){
            throw new UpdateFalseException();
        }
        return new SuccessMessage();
    }
}