<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/4/1 10:28
 */

namespace app\agent\controller\api;


use app\lib\exception\DeleteFalseException;
use app\lib\exception\NoneException;
use app\lib\exception\SuccessMessage;
use app\lib\validate\IDMustBePositiveInt;
use think\Request;

class User extends Base{

    public function del(Request $request){
        (new IDMustBePositiveInt())->goCheck();
        if(\app\agent\model\User::destroy($request->param('id'))){
            return new SuccessMessage();
        }
        throw new DeleteFalseException();
    }



}