<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/11 11:41
 */

namespace app\agent\controller\api;


use app\lib\enum\Code;
use app\lib\exception\FalseException;
use app\lib\exception\SuccessMessage;
use app\lib\exception\UpdateFalseException;
use app\lib\validate\ConfirmRequestValidate;
use app\lib\validate\IDMustBePositiveInt;
use app\qqbot\service\myservice\SendMessage;
use think\Db;

class Drawing{
    public function confirm(\think\Request $request):SuccessMessage{

        (new IDMustBePositiveInt())->goCheck();
        (new ConfirmRequestValidate())->goCheck();
        $request=$request->post();

        $user=\app\agent\model\Drawing::confirmDrawing($request);

        if(!$user){
            throw new UpdateFalseException();
        }

        $status=SendMessage::reduceAfterApplyFromGroup($request['qq'],$request['from_group'],$request['money'],$user->money);

        if(!$status){
            throw new FalseException(['msg'=>'消息回调失败']);
        }

        return new SuccessMessage();
    }

    public function refuse(\think\Request $request){

        (new IDMustBePositiveInt())->goCheck();
        Db::startTrans();
        try{
            \app\agent\model\Drawing::where(['id'=>$request->post('id')])->update(['status'=>Code::O_FAIL]);
            Db::name('user')->where('id',$request->post('user_id'))->inc('money',$request->post('money'))->update();
            Db::commit();
        }catch(\Exception $e){
            Db::rollback();
            throw new UpdateFalseException();
        }

        return new SuccessMessage();
    }
}