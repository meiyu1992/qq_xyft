<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/6 13:59
 */

namespace app\agent\controller;


use app\lib\enum\Admin;
use think\Controller;

class BaseController extends Controller{
    protected $userName;

    protected $page='';
    protected $size='';
    protected $from=0; //查询条件的起始值
    public function initialize() {
        // 判定用户是否登录
        $isLogin = $this->isLogin();
        if(!$isLogin) {
            return $this->redirect('/login');
        }
        $this->userName=$isLogin;

    }


    public function isLogin() {
        //获取session
        $user = session(Admin::QQ_AGENT, '', Admin::QQ_AGENT_SCOPE);

        if($user) {
            return $user;
        }

        return false;
    }
    public function getPageAndSize($data) {
        $this->page = !empty($data['page']) ? $data['page'] : 1;
        $this->size = !empty($data['size']) ? $data['size'] : config('paginate.list_rows');
        $this->from = ($this->page - 1) * $this->size;
    }
}