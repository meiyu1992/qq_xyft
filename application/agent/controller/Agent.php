<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/12 17:58
 */

namespace app\agent\controller;


use app\agent\controller\api\Base;
use app\agent\model\Admin;
use app\agent\model\User;
use app\lib\enum\QqBot;
use app\lib\enum\Salt;
use app\lib\exception\FalseException;
use app\lib\exception\SuccessMessage;
use app\lib\exception\UpdateFalseException;
use app\lib\validate\IDMustBePositiveInt;
use app\lib\validate\QunValidate;
use think\db\Where;
use think\facade\Request;

class Agent extends BaseController{

    public function index(Request $request){
        $data=redisObj()->get(QqBot::AGENT_LIST);
        return $this->fetch('',['data'=>$data]);

    }


    public function add(Request $request){
        if($request::isPost()){
            $arr['qun']=$request::post('qun');
            $arr['qq']=$request::post('qq');

                $data=redisObj()->get(QqBot::AGENT_LIST);
                foreach($data as $k=>$v){
                    if($k==$arr['qun']){
                        echo '<script>alert("该代理群已经群在");window.history.back();</script>';exit;
                    }
                }
                $data[$arr['qun']]=$arr['qq'];
                if(redisObj()->set(QqBot::AGENT_LIST,$data)){
                    $user['name']=$arr['qq'];
                    $user['email']=$arr['qq'].'@qq.com';
                    $user['password']=md5('123456'.Salt::LOGIN_AGENT);//初始密码
                    $user['from_group']= $arr['qun'];
                    $user['o_password']='123456';
                    Admin::create($user);
                    echo '<script>alert("添加成功");parent.location.href="/agent";</script>';
                }

        }

        return $this->fetch();
    }

    public function del(\think\Request $request){
        (new QunValidate())->goCheck();

        try{
            $data=redisObj()->get(QqBot::AGENT_LIST);
            foreach($data as $k=>$v){
                if($k==$request->param('qun')){
                    unset($data[$k]);
                    break;
                }
            }
            redisObj()->set(QqBot::AGENT_LIST,$data);
            Admin::where('name',$request->param('qq'))->delete();
        }catch(\Exception $e){
            throw new UpdateFalseException();
        }

        return new SuccessMessage();
    }

    public function updatePassword(\think\Request $request){


        $data=model('Admin')->group('name')->select();
        return $this->fetch('',['data'=>$data]);


    }

    public function e_p(\think\Request $request){

       if($id=$request->route('id')){
           $data=model('Admin')->group('name')->find($id);
            return $this->fetch('',['data'=>$data]);
       }

       if($request->isPost()){
           $p=$request->post('password');
           if($p && (strlen($p)<6) && (strlen($p)>16)){
               echo '<script>alert("密码不能为空，并且长度不能小于6位或大于16位");parent.location.href="/agent";</script>';exit;
           }

           $password=md5($p.Salt::LOGIN_AGENT);
           if(Admin::where('name',$request->post('qq'))->update(['password'=>$password,'o_password'=>$p])){
               echo '<script>alert("修改成功");parent.location.href="/agent";</script>';exit;
           }
           echo '<script>alert("修改失败");</script>';exit;
       }


        return $this->fetch();
    }
}