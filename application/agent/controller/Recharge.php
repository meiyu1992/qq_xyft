<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/6 17:54
 */

namespace app\agent\controller;
use app\lib\enum\FieldsChange;
use app\lib\enum\QqBot;
use think\db\Where;

class Recharge extends BaseController{

    public function index(){
        $rechargeModel=new \app\agent\model\Recharge();

        $data = input('param.');
        $query=http_build_query($data);

        $this->getPageAndSize($data);

        $whereData=new Where();
        if(!empty($data['start_time']) && !empty($data['end_time'])
            && $data['end_time'] > $data['start_time']
        ) {
            $whereData['a.create_time'] = [
                ['gt', $data['start_time']],
                ['lt', $data['end_time']],
            ];
        }
        if(!empty($data['from'])) {
            $whereData['a.from'] = intval($data['from']);
        }
        if(!empty($data['qq'])) {
            $whereData['a.qq'] = ["like", "%".$data['qq']."%"];
        }
//        $agentList=redisObj()->get(QqBot::AGENT_LIST);
//        $qqQun='';
//
//        foreach($agentList as $k=>$v){
//            if($v == $this->userName){
//                $qqQun=$k;
//            }
//        }

        $recharge=$rechargeModel->getRechargeCondition($whereData,$this->from,$this->size);
        $total =$rechargeModel->getRechargeCountByCondition($whereData);
        $pageTotal=ceil($total/$this->size);

         $result=$this->fetch('',[
             'total'=>$total,
             'from'=>FieldsChange::from(),
             'fromValue'=>empty($data['from'])?'':$data['from'],
             'data'=>$recharge,
             'pageTotal'=>$pageTotal,
             'curr'=>$this->page,
             'start_time'=>empty($data['start_time'])?'':$data['start_time'],
             'end_time'=>empty($data['end_time'])?'':$data['end_time'],
             'qq'=>empty($data['qq'])?'':$data['qq'],
             'query'=>$query
        ]);


        return $result;
    }


}