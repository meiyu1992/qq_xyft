<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/6 17:25
 */

namespace app\agent\model;


use app\lib\enum\Code;
use think\Model;

class Admin extends Base{

    public static function login(string $username,string $pass){
        if($user=self::where('name',$username)->where('back_level',Code::AGENT_BACKSTAGE)->find()){
            if($user->password == $pass){
                return $user;
            }
            return false;
        }
        return false;
    }
}