<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/6 16:25
 */

namespace app\agent\model;


use think\Model;

class User extends Base{

    public function getRechargeCondition($condition=[],$from=0,$size=15){

        $result=$this
            ->where($condition)
            ->limit($from,$size)
            ->group('id')
            ->field('id,qq,money,from,from_group,trueth,create_time')
            ->order('id','desc')
            ->select()->toArray();

        return $result;
    }
    public static function login(string $username,string $pass){
        if($user=self::where('name',$username)->find()){
            if(!($user->password<=>$pass)){
                return false;
            }
            return true;
        }
        return false;
    }

    public function getTruethAttr($v){
        $status=[1=>'真',2=>'假'];
        return $status[$v];
    }
}