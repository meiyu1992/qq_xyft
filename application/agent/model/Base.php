<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/6 16:25
 */

namespace app\agent\model;


use think\Model;
use think\model\concern\SoftDelete;

class Base extends Model{
    use SoftDelete;
    protected $hidden=['update_time'];

    public function getStatusAttr($v){
        $status = [1=>'待确认',2=>'成功',3=>'失败'];
        return $status[$v];
    }
}