<?php
/**
 * User: 269995848@qq.com
 * Date: 2019/3/11 2:37
 */

namespace app\agent\model;


use app\lib\enum\Code;
use think\Db;

class Drawing extends Base{

    //下分
    public function getRechargeCondition($condition=[], $qqQun,$from=0,$size=15){

        $result=$this->alias('a')
            ->join('user b','a.member_id=b.id')
            ->where('a.from_group',$qqQun)
            ->where($condition)
            ->limit($from,$size)
            ->group('a.id')
            ->field('a.id,a.bill_no,a.member_id,a.name,a.qq,a.money,a.payment_type,a.from,a.from_group,a.status,a.user_id,a.create_time,b.trueth,b.money as current_money')
            ->order('a.id','desc')
            ->select()->toArray();

        return $result;
    }

    public function getRechargeCountByCondition($param=[]){
        return $this->count();
    }

    public static function confirmDrawing(array $re) {


        Db::startTrans();
        try{
            $user_id=\think\facade\Cache::get('admin_name')??'';
            self::where('id', $re['id'])->update(['status'=>Code::O_SUCCESS,'user_id'=>$user_id]);
            $user=User::get($re['member_id']);
            Db::commit();
            return $user;
        }catch(\Exception $e){
            Db::rollback();
            return false;
        }

    }
}