<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
/*
     * 自定义的返回数据格式,
     * return array
     * */

use app\lib\enum\FieldsChange;


//获取状态名称
function getStatusName($statusID){
    if(!$statusID){
        return '';
    }
    $status=FieldsChange::status();
    return !empty($status[$statusID])?$status[$statusID]:'';
}

//获取来源名称 QQ群或者盘口
function getFromName($fromID){
    if(!$fromID){
        return '';
    }
    $status=FieldsChange::from();
    return !empty($status[$fromID])?$status[$fromID]:'';
}

//获取交易平台
function getPlatName($platformID){
    if(!$platformID){
        return '';
    }
    $status=FieldsChange::platform();
    return !empty($status[$platformID])?$status[$platformID]:'';
}

//获取真假用户
function isReallyUser($changeID){
    if(!$changeID){
        return '';
    }
    $content=FieldsChange::trueTh();
    return !empty($content[$changeID])?$content[$changeID]:'';
}


function returnData($data=[]){
    if(!$data){
        return response(['error_code'=>0,'data'=>['msg'=>'ok']]);
    }
    return response(['error_code'=>0,'data'=>$data]);
}

//非接口的形式返回


function send_post($url,$data){
    $curl=curl_init();
    curl_setopt($curl,CURLOPT_URL,$url);

    curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,false);
    curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,false);

    if(!empty($data)){
        curl_setopt($curl,CURLOPT_POST,1);
        curl_setopt($curl,CURLOPT_POSTFIELDS,$data);
    }

    curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);

    $output=curl_exec($curl);

    curl_close($curl);

    return $output;
}

function send_get($url){
    //初始化
    $curl = curl_init();
    //设置抓取的url
    curl_setopt($curl, CURLOPT_URL, $url);
    //设置头文件的信息作为数据流输出
    curl_setopt($curl, CURLOPT_HEADER, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
    //设置获取的信息以文件流的形式返回，而不是直接输出。
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    //执行命令
    $result = curl_exec($curl);
    if($result === false){
        echo curl_errno($curl);

    }
    //关闭URL请求
    curl_close($curl);
    //显示获得的数据
    return $result;
}

function makeOrderNo(){
    $yCode=array('A','B','C','D','E','F','G','H','I','J');
    $orderSn=$yCode[intval(date('Y'))-2019].strtoupper(dechex(date('m'))).date('d').substr(time(),-5).substr(microtime(),2,5).sprintf('%02d',rand(0,99));
    return $orderSn;
}

function redisObj(){
    return new \app\common\Redis(config('redis.'));
}

function isInt($else){
    if((int)$else && is_numeric($else) && is_int($else + 0) && ($else + 0) > 0){
        return true;
    }
    return false;
}

function is_serialized($data):bool{
    $data=trim($data);
    if('N;'==$data)
        return true;
    if(!preg_match('/^([adObis]):/',$data,$badions))
        return false;
    switch($badions[1]){
        case 'a' :
        case 'O' :
        case 's' :
            if(preg_match("/^{$badions[1]}:[0-9]+:.*[;}]\$/s",$data))
                return true;
        break;
        case 'b' :
        case 'i' :
        case 'd' :
            if(preg_match("/^{$badions[1]}:[0-9.E-]+;\$/",$data))
                return true;
        break;
    }
    return false;
}

function echoMsg($msg,$url=''){

    echo '<script>alert("'.$msg.'");parent.location.href="'.$url.'";</script>';exit;
}


