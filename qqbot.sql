CREATE TABLE `user`  (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `qq` int(16) NOT NULL DEFAULT 0 COMMENT 'q群用户',
  `from_group` int(16) NULL DEFAULT NULL COMMENT '所在Q群',
  `webuser` int(16) NULL DEFAULT NULL COMMENT '盘口用户',
  `trueth` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '1真用户2假',
  `webpassword` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '盘口用户密码',
  `money` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '积分',
  `from` tinyint(2) NOT NULL DEFAULT 1 COMMENT '用户来源，1Q群2盘口3其他',
  `o_pass` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE (`qq`) USING BTREE
) ENGINE = InnoDB;



CREATE TABLE `admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `back_level` int(1) null default 2 comment '后台等级',
  `from_group` varchar(15) null default null comment '该代理所属群',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_super_admin` tinyint(4) NOT NULL DEFAULT '0',
  `role_id` int(11) NOT NULL DEFAULT '1' COMMENT '角色 id 1游客',
  `last_session` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `o_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB;

CREATE TABLE `drawing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bill_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '交易流水号',
  `member_id` int(11) NOT NULL COMMENT '用户id',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '充值人名称、昵称',
  `qq` varchar(15) DEFAULT NULL COMMENT '充值人qq',
  `from` tinyint(2) not null default '1' comment '来源平台，1Q群2盘口3其他',
  `money` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '提款金额',
  `payment_desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '账户 支付宝账户 微信账户 银行卡号',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1待确认2成功3失败',
  `after_money` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '提款后金额',
  `counter_fee` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '手续费',
  `fail_reason` text COLLATE utf8mb4_unicode_ci COMMENT '失败原因',
  `bank_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '银行名称',
  `bank_card` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '银行卡号',
  `confirm_at` timestamp NULL DEFAULT NULL COMMENT '确认提款成功时间',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '管理员账号',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=innodb;

CREATE TABLE `recharge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bill_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '交易流水号',
  `member_id` int(11) default NULL COMMENT '用户id',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '充值人名称、昵称',
  `qq` varchar(15) DEFAULT NULL COMMENT '充值人qq',
  `money` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '充值金额',
  `payment_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '转账类型 1支付宝2微信3银行转账',
  `from` tinyint(2) not null default '1' comment '来源平台，1Q群2盘口3其他',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1待确认2充值成功3充值失败',
  `diff_money` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '赠送金额',
  `before_money` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '充值前金额',
  `after_money` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '充值后金额',
  `fail_reason` text COLLATE utf8mb4_unicode_ci COMMENT '失败原因',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '管理员账号',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=innodb;

CREATE TABLE `base_setting` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `welcome` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_private` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `atqqbot` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=INNODB;

CREATE TABLE `game_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL comment '下注类型，1幸运飞艇',
  `qq` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from_group` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL comment '所属Q群',
  `money` varchar(15) not null default 0 comment '下注金额',
  `bet_code` varchar(255) not null default '' comment '下注号码',
  `trueth` varchar(1) not null default 1 comment '真假注 1true 0 false',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB;

CREATE TABLE `game_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rowid` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `billNo` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '注单流水号',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '玩家昵称',
  `qq` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '玩家qq',
  `member_id` int(11) NOT NULL COMMENT '用户 ID',
  `netAmount` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '玩家输赢额度',
  `platAmount` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '平台输赢额度',
  `betTime` timestamp NULL DEFAULT NULL COMMENT '投注时间',
  `count` tinyint(10) NULL DEFAULT 0 comment '每笔下注次数',
  `gameType` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '游戏类型',
  `betAmount` decimal(16,2) DEFAULT '0.00' COMMENT '投注金额',
  `validBetAmount` decimal(16,2) DEFAULT '0.00' COMMENT '有效投注额度',
  `flag` int(11) DEFAULT '0' COMMENT '注单状态',
  `xzhm` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '下注号码',
  `odds` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '赔率',
  `oddsType` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '赔率类型',
  `netPnl` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '净输赢',
  `settleTime` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '结算时间',
  `reAmount` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '备用',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `game_record_billno_index` (`billNo`),
  KEY `game_record_qq_index` (`qq`),
  KEY `game_record_playername_index` (`name`),
  KEY `game_record_bettime_index` (`betTime`),
  KEY `game_record_gametype_index` (`gameType`)
) ENGINE=innodb;