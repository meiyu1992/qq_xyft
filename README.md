qq机器人搭建

PS：基于tp5.1,必须安装redis,phantomJs。支持把Q群做为一个代理群

##  基础配置
### 1.先让机器人监听本地端口

例子
{
    "host": "0.0.0.0",
    "port": 5700,
    "use_http": true,
    "ws_host": "",
    "ws_port": "",
    "use_ws": false,
    "ws_reverse_url": "",
    "ws_reverse_api_url": "",
    "ws_reverse_event_url": "",
    "ws_reverse_reconnect_interval": 3000,
    "ws_reverse_reconnect_on_code_1000": true,
    "use_ws_reverse": false,
    "post_url": "http://127.0.0.1:80/qqbot/Index/index",
    "access_token": "",
    "secret": "",
    "post_message_format": "string",
    "serve_data_files": false,
    "update_source": "github",
    "update_channel": "stable",
    "auto_check_update": false,
    "auto_perform_update": false,
    "show_log_console": false,
    "log_level": "error",
    "enable_rate_limited_actions":true,
    "rate_limit_interval":1000
}

### 2.让本地监听机器人端口

例
<VirtualHost _default_:80>
DocumentRoot "your demo local"
  <Directory "your demo local">
    Options -Indexes -FollowSymLinks +ExecCGI
    AllowOverride All
    Order allow,deny
    Allow from all
    Require all granted
  </Directory>
</VirtualHost>

## 目录结构

## 初始的目录结构如下：

ps:admin,agent模块内容很少，现在主要的精力放在qqbot模块

~~~
www  WEB部署目录（或者子目录）
├─application           应用目录
│  ├─common             公共模块目录（可以更改）
│  ├─admin              总后台模块目录，目前几乎没有功能
│  ├─agent              代理后台，少许的功能
│  ├─qqbot              机器人模块
│  │ └─service          核心业务类
│  │    └─command       指令存放区域
│  └─lib    
│    └─enum             项目配置区
│       └─QqBot.php     机器人配置类         
│
├─采集.bat
~~~

## 测试

### 开启采集
命令行:php think qqbot_caiji start  或者双击 采集.bat 

### 帮助
在Q群输入“帮助” 或者 “!帮助” 即可

### just do it



